<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admin profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/admin" class="profile">Administration</a></li>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum">Forum</a></li>
        <li><a href="/profile/${us}">Profile</a></li>
        <li><a href="/logout">Out</a></li>
    </ul>
</div>

<div class="add_news">
    <form class="form-news" role="form">
        <h2>Create news</h2>
        <input type="text" class="form-control" placeholder="Title" required autofocus>
        <input type="text" class="form-control" placeholder="News" required autofocus>
        <p class="main_p">Main photo</p>
        <form enctype="multipart/form-data" method="post">
            <p class="main_photo"><input class="ch" type="file" name="f">
                <#--<input class="Send" type="submit" value="Отправить"></p>-->
        </form>
<#--
        <p class="news_photos">News photos</p>
        <form enctype="multipart/form-data" method="post">
            <p class="news_slider_photo1"><input class="ch" type="file" name="f">
                <input class="Send" type="submit" value="Отправить"></p>
        </form>
        <form enctype="multipart/form-data" method="post">
            <p class="news_slider_photo2"><input class="ch" type="file" name="f">
                <input class="Send" type="submit" value="Отправить"></p>
        </form>
        <form enctype="multipart/form-data" method="post">
            <p class="news_slider_photo3 "><input class="ch" type="file" name="f">
                <input class="Send" type="submit" value="Отправить"></p>
        </form>
-->
        <button class="button" type="submit">Create</button>
    </form>
</div>

<div class="add_artist">
    <form class="form-artist" role="form">
        <h2>Create artist</h2>
        <input type="text" class="form-control" placeholder="Name" required autofocus>
        <input type="text" class="form-control" placeholder="Info" required autofocus>
        <input type="text" class="form-control" placeholder="Link to video" required autofocus>

        <p class="main_p">Main photo</p>
        <form enctype="multipart/form-data" method="post">
            <p class="main_photo"><input class="ch" type="file" name="f">
                <#--<input class="Send" type="submit" value="Отправить"></p>-->
        </form>

        <p class="artist_photos">Artists photos</p>
        <form enctype="multipart/form-data" method="post">
            <p class="artist_slider_photo1"><input class="ch" type="file" name="f">
                <#--<input class="Send" type="submit" value="send"></p>-->
        </form>
        <form enctype="multipart/form-data" method="post">
            <p class="artist_slider_photo2"><input class="ch" type="file" name="f">
                <#--<input class="Send" type="submit" value="Отправить"></p>-->
        </form>
        <form enctype="multipart/form-data" method="post">
            <p class="artist_slider_photo3 "><input class="ch" type="file" name="f">
                <#--<input class="Send" type="submit" value="Отправить"></p>-->
        </form>

        <button class="button" type="submit">Create</button>
    </form>
</div>

<div class="add_admin">
    <form class="form-admin" role="form" action="/admin" method="post">
        <h2>Create admin</h2>
        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
        <br/>
        <button type="submit">Create</button>
    </form>
</div>

</body>
</html>