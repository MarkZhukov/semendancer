<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>News</title>
    <link href="../../css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<div class="navigation">
    <ul>
    <#if is_admin == 1>
        <li><a href="/admin">Administration</a></li>
    </#if>
        <li><a href="/home" class="news">News</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum">Forum</a></li>
    <#if isLogIn == 1>
        <li><a href="/profile/${us}">Profile</a></li>
        <li><a href="/logout">Out</a></li>
    <#else >
        <li><a href="/login">Sign in</a></li>
    </#if>

</ul>
</div>

<div class="news">
    <#list new as new>
    <div class="card mb-3">
        <img class="card-img-top" src="${new.mainPhoto}" alt="">
        <div class="card-body">
            <h4 class="card-title">${new.headLine}</h4>
            <p class="card-text">${new.description}</p>
            <p class="card-text"><small class="text-muted">${new.dt}</small></p>
            <#if is_admin == 1>
            <div class="delete">
                <a href="/delete/news/${new.id}"><img src="../../img/delete.png" width="25" height="25"></a>
            </div>
            </#if>
        </div>
    </div>
    </#list>

    <#include "footer.ftl">
</body>
</html>