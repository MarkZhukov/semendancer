<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <link href="../../css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <title>${topic.title}</title>
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum" class="forum">Forum</a></li>
    <#if isLogIn == 1>
        <li><a href="/profile/${us.username}">Profile</a></li>
        <li><a href="/logout">Out</a></li>
    <#else >
        <li><a href="/login">Sign in</a></li>
    </#if>
    </ul>
</div>
<#--forum_search-->
<div class="topic">
    <blockquote>
        <div class="forum_search">
        <h2>${topic.title}</h2>
        </div>
    </blockquote>
</div>

<#if comments?has_content>
    <#list comments as c>
<div class="topic">
    <a href="/profile/${c.owner.username}">
        <#if c.owner.photo?has_content>
            <img src="${c.owner.photo}" alt="" class="img-rounded" width="auto" height="150">
        <#else>
            <img src="../../img/0.jpg" alt="" class="img-rounded" width="auto" height="150">
        </#if>
    <blockquote>
        <p>${c.description}</p>
        <#--<p class="dmy">DD/MM/YY</p>-->
        <footer>${c.owner.username}</cite></footer>
        <#if is_admin == 1>
        <div class="delete">
            <a href="/delete/comment/${c.id}"><img src="../../img/delete.png" width="25" height="25"></a>
        </div>
        </#if>
    </blockquote>
        </a>
    </div>
    </#list>
</#if>
<#--
<div class="topic">
    <img src="../../img/get_in.jpg" alt="..." class="img-rounded" width="auto" height="150">

    <blockquote>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
        <p class="dmy">DD/MM/YY</p>
        <footer>Author</cite></footer>
        <div class="delete">
            <a href="#"><img src="../../img/delete.png" width="25" height="25"></a>
        </div>
    </blockquote>
</div>
-->
<#if us?has_content>
<div class="topic">

<#if us.photo?has_content>
<img src="${us.photo}" alt="" class="img-rounded" width="auto" height="150">
<#else >
    <img src="../../img/0.jpg" alt="" class="img-rounded" width="auto" height="150">
</#if>
    <blockquote>
        <div class="topic_form">
            <form action="/forum/topic/${topic.id}" method="post">
                <input type="text" name="comment" class="form-control" placeholder="Comment..." required autofocus>
                <button class="comment" type="submit">Comment on</button>
            </form>
        </div>
</blockquote>

</div>
<#else>
<div class="forum_search">
    <a href="/login">Log in if you wanna drop comment</a>
</div>
</#if>

</body>
</html>