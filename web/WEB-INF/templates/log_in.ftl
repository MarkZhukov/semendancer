<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sign in</title>
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">Home</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum">Forum</a></li>
        <li><a href="/login" class="sign_in">Sign in</a></li>
        <li><a href="/signIn">Sign up</a></li>
    </ul>
</div>

<div class="container">
    <form class="form-signin" role="form" method="post" action="/login">
        <h2>Sign in</h2>
        <input type="text" class="form-control" name="username" placeholder="username" required autofocus>
        <input type="password" class="form-control" name="password" placeholder="password" required autofocus>
        <label class="checkbox">
            <input type="checkbox" value="remember-me" name="remember"> Remember me
        </label>
        <br/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">sign in</button>
    </form>
</div>

<#include "footer.ftl">
</body>
</html>