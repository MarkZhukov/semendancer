<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sign up</title>
    <link href="../../css/style.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum">Forum</a></li>
        <li><a href="/login">Sign in</a></li>
        <li><a href="/signIn" class="sign_up">Sign up</a></li>
    </ul>
</div>

<div class="sign_up">
    <form class="form-signup" role="form" enctype="multipart/form-data" action="/signIn" method="post">
        <h2 class="form-signup-heading">Sign up</h2>
        <input type="text" class="form-control" placeholder="name" name="realName" required autofocus>
        <input type="text"  class="form-control" placeholder="username" name="username" required autofocus>
        <input type="password" class="form-control" placeholder="password" name="password1" required autofocus>
        <input type="password" class="form-control" placeholder="confirm password" name="password2" required autofocus>
        <input type="text" class="form-control" placeholder="city" name="city" autofocus>
        <input type="text" class="form-control" placeholder="about" name="about" autofocus>
        <p>affix photo</p>
        <input type="file" class="form-control" name="photo">
        <p>links</p>
        <input type="text" class="form-control" placeholder="VK" name="vk" autofocus>
        <input type="text" class="form-control" placeholder="Facebook" name="face" autofocus>
        <input type="text" class="form-control" placeholder="Instagram" name="inst" autofocus>
        <input type="text" class="form-control" placeholder="Twitter" name="twit" autofocus>
        <button class="btn btn-lg btn-primary btn-block" type="submit">sign up</button>
    </form>
</div>

<#include "footer.ftl">
</body>
</html>