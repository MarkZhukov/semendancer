<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Get_in!</title>
    <link href="../../css/stylesheet.css" rel="stylesheet">
</head>
<body>
<img width="100%" height="100%" src="../../img/get_in.jpg">
<h2>Dance is the hidden language of the soul</h2>
<form action="/start" method="POST">
    <button>Get in!</button>
</form>
<#include "footer.ftl">
</body>
</html>