<!doctype html>
<html lang="">

<head>
    <meta charset="utf-8">
    <title>Artist</title>
    <link href="../../css/style.css" rel="stylesheet" type="text/css">
    <script src="../../js/javascript.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="">
    <script type="text/javascript" src="/js/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list" class="artists">Artists</a></li>
        <li><a href="/forum">Forum</a></li>
    <#if isLogIn == 1>
        <li><a href="/profile/${us}">Profile</a></li>
        <li><a href="/logout">Out</a></li>
    <#else>
        <li><a href="/login">Sign in</a></li>
    </#if>
    </ul>
</div>

<div class="artist">
    <div class="table">
        <img class="photo" src="${artist.mainPhoto}">
        <div>
            <h3>${artist.name}</h3>
            <p>${artist.info_url}</p>
        </div>



        <div id="slider" class="carousel slide" data-ride="carousel">
            <#--
            <ol class="carousel-indicators">
                <li data-target="#slider" data-slide-to="0" class="active"></li>
                <li data-target="#slider" data-slide-to="1"></li>
                <li data-target="#slider" data-slide-to="2"></li>
            </ol>
        -->

            <div class="carousel-inner">

                <div class="carousel-item active">
                    <img class="d-block w-100"
                         src="${tit}"
                         alt="">
                    <div class="carousel-caption d-none d-md-block">
                        <h3></h3>
                        <p></p>
                    </div>
                </div>
    <#if photos?has_content>
                <#list photos as img>
                <div class="carousel-item">
                    <img class="d-block w-100"
                         src="${img}"
                         alt="">
                    <div class="carousel-caption d-none d-md-block">
                        <h3></h3>
                        <p></p>
                    </div>
                </div>
        </#list>
                <#--<div class="carousel-item">
                    <img class="d-block w-100"
                         src="https://www.lacity.org/sites/g/files/wph781/f/styles/tiled_homepage_blog/public/bigstock-Los-Angeles-Skyline-And-Freewa-4583042.jpg?itok=DYo1GpRC"
                         alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h3>Hey 3 </h3>
                        <p>Test 3</p>
                    </div>
                </div>-->
            </div>
            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </#if>
<#if video?has_content>
    <iframe width="1100" height="650" src="${video}" frameborder="0" allowfullscreen></iframe>
</#if>

</body>
</html>












<#--
<body>
<h3>Artist</h3>
<br>

<a href="/home">home</a>
<a href="/forum">forum</a>
<a href="/artist_list">personal</a>

<#if isLogIn == 1>
<a href="/profile">profile</a>
<a href="/logout">Out</a>
    <#if is_admin == 1>
    <br><hr>
    <a>delete him</a>
    </#if>
<#else>
<a href="/login">log in</a>
<a href="/signIn">sign in</a>

</#if>
<hr>
<img src="" width="550" height="300" alt="no photo"><h3></h3>
<p></p>
<ul>

<iframe width="560" height="315"
        src=""
        frameborder="0" gesture="media" allowfullscreen>
</iframe>
</#if>



<#if is_admin == 1>
<p>i'm admin and i can add and delete artistes</p>
</#if>

</body>-->

<#--https://www.youtube.com/embed/INIZbxJ7eHE-->
