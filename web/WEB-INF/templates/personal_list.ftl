<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Artists</title>
    <link href="../../css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list" class="artists">Artists</a></li>
        <li><a href="/forum">Forum</a></li>
            <#if isLogIn == 1>
                <li><a href="/profile/${us}">Profile</a></li>
                <li><a href="/logout">Out</a></li>
            <#else>
                <li><a href="/login">Sign in</a></li>
            </#if>
    </ul>
</div>

<div class="artists_0">
<#if artistes?has_content>
    <#list artistes as art>
    <a href="/artist_list/artist/${art.id}">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-2">
                <div class="table">
        <#if art.mainPhoto?has_content>
                    <img src="${art.mainPhoto}" width="100" height="100">
        <#else>
                    <img src="../../img/0.jpg" width="100" height="100">
        </#if>
                </div>
            </div>
            <div class="col-md-3">
                <p>${art.name}</p>
            </div>
            <#if is_admin == 1>
            <div class="col-md-2">
                <div class="delete">
            <a href="/delete/art/${art.id}"><img src="../../img/delete.png" width="25" height="25"></a>
                </div>
            </div>
            </#if>
            <div class="col-md-4"></div>
        </div>
    </a>
    </#list>
<#else>
    <p>No artistes</p>
</#if>
</div>

<#include "footer.ftl">
</body>
</html>