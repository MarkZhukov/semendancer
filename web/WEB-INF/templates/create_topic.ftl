<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Create topic</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum" class="forum">Forum</a></li>
        <li><a href="/profile/${us}">Profile</a></li>
        <li><a href="/logout">Out</a></li>
    </ul>
</div>

<div class="create_topic">
    <form class="form-createtopic" role="form" method="post" action="/forum/create_topic">
        <h2>Creating a topic</h2>
        <input type="text" name="title" class="form-control" placeholder="Title" required autofocus>
        <input type="text" name="text" class="form-control" placeholder="Text" required autofocus>
        <p>input like that</p>
        <p>#tag #dance #input</p>
        <input type="text" name="tags" class="form-control" placeholder="Search tags" required autofocus>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Create topic</button>
    </form>
</div>

<#include "footer.ftl">
</body>
</html>