<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Forum</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum" class="forum">Forum</a></li>
        <#if isLogIn == 1>
            <li><a href="/profile/${us}">Profile</a></li>
            <li><a href="/logout">Out</a></li>
        <#else >
            <li><a href="/login">Sign in</a></li>
        </#if>
    </ul>
</div>


<div class="forum">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <div class="input-group">
            <#if isLogIn == 1>
                <form method="get" action="/forum/create_topic">
                    <button class="btn btn-secondary" type="submit">Create topic</button>
                </form>
            </#if>
                <form method="post" action="/forum">
                    <input type="text" id="s_id" name="search" class="form-control"  placeholder="" aria-label="Search" oninput="f()">
                    <button class="btn btn-secondary" type="submit">Search</button>
                </form>
            </div>
        </div>
    </div>
</div>

<#if topics?has_content>
    <#list topics as t>
<div class="topic_1">
    <h4><a href="/forum/topic/${t.id}">${t.title}</a></h4>
    <br>
    <p>${t.comment.description}</p>
        <#if is_admin == 1>
    <div class="delete">
        <a href="/delete/topic/${t.id}"><img src="../../img/delete.png" width="25" height="25"></a>
    </div>
    </#if>
</div>
    </#list>
<#else>
<div class="forum_search">
    <p>Nothing found</p>
</div>
</#if>



<div id="results"></div>
<script type="application/javascript">
    function f() {
        console.log(1);
        $.ajax({
            url: "/forum",
            data: JSON.parse({'search': $("#s_id").val()}),
            dataType: "json",
            success: function (result) {

                for (var i = 0; i < result.topics.length; i++) {
                    $("#results").append("<li>" + result.topics[i] + "</li>");
                }
            },
            error: function (jqXHR, exception) {

                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            },
        });
    }
</script>

<#include "footer.ftl">
</body>
</html>