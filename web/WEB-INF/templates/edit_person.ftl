<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Edit profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../../css/style.css" rel="stylesheet">
</head>
<body>
<div class="navigation">
    <ul>
        <li><a href="/home">News</a></li>
        <li><a href="/artist_list">Artists</a></li>
        <li><a href="/forum">Forum</a></li>
        <li><a href="/profile" class="profile">Profile</a></li>
        <li><a href="/logout">Out</a></li>
</div>

<div class="edit_profile">
    <form class="form-edit" role="form">
        <input type="text" class="form-control" placeholder="${p.realName}" required autofocus>
        <input type="password" class="form-control" placeholder="Password" required autofocus>
        <input type="password" class="form-control" placeholder="Confirm password" required autofocus>
        <input type="text" class="form-control" placeholder="${p.city}" autofocus>
        <input type="text" class="form-control" placeholder="${p.description}" autofocus>
        <p>Links to the social network:</p>

        <#--<input type="text" class="form-control" placeholder="${p.vkLink}" autofocus>-->
        <#--<input type="text" class="form-control" placeholder="${p.faceLink}" autofocus>-->
        <#--<input type="text" class="form-control" placeholder="${p.instLink}" autofocus>-->
        <#--<input type="text" class="form-control" placeholder="${p.twitLink}"  autofocus>-->
        <p>Upload a photo</p>
        <form enctype="multipart/form-data" method="post" action="/edit_person/${p.id}">
            <p><input type="file" name="f">
                <input type="submit" value="send"></p>
        </form>
        <form enctype="multipart/form-data" method="get" action="/profile">
            <button class="button" type="submit">Cancel</button>
        </form>

    </form>

</div>

</body>
</html>
















<!doctype html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h3>View Person</h3>
<br>
<a href="/home">home</a>
<a href="/artist_list">personal</a>
<a href="/forum">forum</a>


<#if isLogIn == 1>
<a href="/profile">profile</a>
<a href="/logout">log out</a>
<#else>
<a href="/login">log in</a>
<a href="/signIn">sign in</a>
</#if>
<hr><br>
<#------------------------------------------------------------------>


<img src="${person.photo}" width="550" height="300" alt="no photo">
<h3>${person.realName}</h3>
<p>${person.username}</p>
<p>${person.description}</p>
<p>${person.city}</p>

<p>links</p>
<p>${person.instLink}, ${person.twitLink}, ${person.faceLink}, ${person.vkLink}</p>







<#if is_admin == 1>
<p>delete</p>
</#if>

</body>
</html