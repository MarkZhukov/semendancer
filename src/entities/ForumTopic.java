package entities;


import service.db.dao.CommentDAO;

/**
 * Created by lucky.mz
 */
public class ForumTopic {
    private int id;
    private String title;
    private int first_comment_id;
    private Comment comment;


    public ForumTopic(int id ,String title ,
                      int first_comment_id) {
        this.id = id;
        this.title = title;
        this.first_comment_id = first_comment_id;
        this.comment = getCommentDB(first_comment_id);
    }

    private Comment getCommentDB(int first_comment_id) {
        CommentDAO commentDAO = new CommentDAO();
        return commentDAO.getById(first_comment_id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFirst_comment_id() {
        return first_comment_id;
    }

    public void setFirst_comment_id(int first_comment_id) {
        this.first_comment_id = first_comment_id;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}
