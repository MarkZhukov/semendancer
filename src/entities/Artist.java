package entities;

import service.db.dao.MediaDAO;

/**
 * Created by lucky.mz case he was not very busy
 */
public class Artist {
    private String name;
    private String info_url;
    private int id;
    private int mainPhoto_id;
    private String mainPhoto;


    public Artist(String name, String info_url, int id, int mainPhoto_id) {
        this.mainPhoto_id = mainPhoto_id;
        this.name = name;
        this.info_url = info_url;
        this.id = id;
        this.mainPhoto = getMainPhotoDB(mainPhoto_id);
    }

    private String getMainPhotoDB(int mainPhoto_id) {
        MediaDAO photoDAO = new MediaDAO();
        return photoDAO.getMainArtPhoto(mainPhoto_id);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo_url() {
        return info_url;
    }

    public void setInfo_url(String info_url) {
        this.info_url = info_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMainPhoto_id() {
        return mainPhoto_id;
    }

    public void setMainPhoto_id(int mainPhoto_id) {
        this.mainPhoto_id = mainPhoto_id;
    }

    public String getMainPhoto() {
        return mainPhoto;
    }

    public void setMainPhoto(String mainPhoto) {
        this.mainPhoto = mainPhoto;
    }
}
