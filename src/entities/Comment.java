package entities;

import service.db.dao.PersonDAO;

import java.util.Calendar;

/**
 * Created
 */
public class Comment {
    private String description;
    private Person owner;
    private int owner_id;
    private int id;
    private int topic_id;
    private Calendar dateTime;


    public Comment(int id, String description,
                   int owner_id, int topic_id, long date) {
        this.id = id;
        this.description = description;
        this.owner_id = owner_id;
        this.owner = getOwnerDB(owner_id);
        this.topic_id = topic_id;
        this.dateTime = Calendar.getInstance();
        this.dateTime.setTimeInMillis(date);
    }

    public Comment(int id, String content, int user_id, int topic_id) {
        this.id = id;
        this.description = content;
        this.owner_id = user_id;
        this.topic_id = topic_id;
        this.owner = getOwnerDB(user_id);
    }

    private Person getOwnerDB(int owner_id) {
        PersonDAO personDAO = new PersonDAO();
        return personDAO.getById(owner_id);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public Calendar getDateTime() {
        return dateTime;
    }

    public void setDateTime(Calendar dateTime) {
        this.dateTime = dateTime;
    }
}
