package entities;

/**
 * Created by lucky.mz case ... why not
 */
public class Person {
    private String username;
    private int id;
    private String photo;
    private String realName;
    private String description;
    private String city;
    private String vkLink;
    private String twitLink;
    private String faceLink;
    private String instLink;

    public Person(String username, int id, String pathToImage, String realName
            , String description, String city, String vkLink
            , String twitLink, String faceLink, String instLink) {
        this.username = username;
        this.id = id;
        this.photo = pathToImage;
        this.realName = realName;
        this.description = description;
        this.city = city;
        this.vkLink = vkLink;
        this.twitLink = twitLink;
        this.faceLink = faceLink;
        this.instLink = instLink;
    }

    public Person(String username, int id) {

    }

    public Person(String username, int id, String img_url, String name, String about, String city) {
        this.username = username;
        this.id = id;
        this.photo = img_url;
        this.realName = name;
        this.description = about;
        this.city = city;
        if (img_url == null) {
            photo = "";
        }
        if (about == null) {
            description = "";
        }
        if (city == null) {
            this.city = "";
        }
    }

    public Person() {

    }




    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVkLink() {
        return vkLink;
    }

    public void setVkLink(String vkLink) {
        this.vkLink = vkLink;
    }

    public String getTwitLink() {
        return twitLink;
    }

    public void setTwitLink(String twitLink) {
        this.twitLink = twitLink;
    }

    public String getFaceLink() {
        return faceLink;
    }

    public void setFaceLink(String faceLink) {
        this.faceLink = faceLink;
    }

    public String getInstLink() {
        return instLink;
    }

    public void setInstLink(String instLink) {
        this.instLink = instLink;
    }
}
