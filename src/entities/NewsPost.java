package entities;

import service.db.dao.MediaDAO;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by lucky.mz case he need it
 */
public class NewsPost {
    private String headLine;
    private String description;
    private int id;
    private Calendar date;
    private String mainPhoto;
    private int main_photo_id;
    private Date time;
    private String dt;

    public NewsPost(int id, String headLine, String description, String date
    ,int main_photo_id) {
        this.headLine = headLine;
        this.description = description;
        this.id = id;
        //this.date = Calendar.getInstance();
        //this.date.setTimeInMillis(date);
        //this.main_photo_id = main_photo_id;
        this.mainPhoto = getMainPhotoDB(main_photo_id);
        //time = Calendar.getInstance().getTime();
        String pat = date;
        dt = "";
        for (int i = 0; i < 16; i++) {
            dt += pat.charAt(i);
        }
    }

    private String getMainPhotoDB(int id) {
        MediaDAO pd = new MediaDAO();
        return pd.getNewMainPhoto(id);
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getMainPhoto() {
        return mainPhoto;
    }

    public void setMainPhoto(String mainPhoto) {
        this.mainPhoto = mainPhoto;
    }

    public int getMain_photo_id() {
        return main_photo_id;
    }

    public void setMain_photo_id(int main_photo_id) {
        this.main_photo_id = main_photo_id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }
}
