CREATE TABLE "Artists" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL,
	"info_url" TEXT NOT NULL,
	"main_photo" integer UNIQUE,
	CONSTRAINT Artists_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "News" (
	"id" serial NOT NULL,
	"title" TEXT NOT NULL UNIQUE,
	"content_url" TEXT NOT NULL,
	"date" DATE NOT NULL,
	"main_photo" integer UNIQUE,
	CONSTRAINT News_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "News_photos" (
	"id" serial NOT NULL,
	"news_id" integer,
	"photo_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT News_photos_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Artist_photo" (
	"id" serial NOT NULL,
	"artist_id" integer,
	"content_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT Artist_photo_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "Artist_video" (
	"id" serial NOT NULL,
	"artist_id" integer,
	"content_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT Artist_video_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);




CREATE TABLE "Users" (
	"id" serial NOT NULL,
	"username" TEXT NOT NULL UNIQUE,
	"name" TEXT NOT NULL,
	"password" TEXT NOT NULL,
	"city" TEXT,
	"about" TEXT,
	"img_url" TEXT,
	CONSTRAINT Users_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "User_links" (
	"id" serial NOT NULL,
	"user_id" integer NOT NULL,
	"link_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT User_links_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Admins" (
	"id" serial NOT NULL,
	"user_id" integer NOT NULL UNIQUE,
	CONSTRAINT Admins_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Topicks" (
	"id" serial NOT NULL,
	"title" TEXT NOT NULL,
	"initial_comment" integer UNIQUE,
	CONSTRAINT Topicks_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Comments" (
	"id" serial NOT NULL,
	"user_id" integer NOT NULL,
	"content" TEXT NOT NULL,
	"date_time" TIMESTAMP,
	"topick_id" integer,
	CONSTRAINT Comments_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Tags" (
	"id" serial NOT NULL,
	"tag" TEXT NOT NULL UNIQUE,
	CONSTRAINT Tags_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Tags_in_topicks" (
	"id" serial NOT NULL,
	"tag_id" integer NOT NULL,
	"topick_id" integer NOT NULL,
	CONSTRAINT Tags_in_topicks_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "Cookies" (
  "id" serial NOT NULL,
  "user_id" integer NOT NULL,
  "hash" TEXT NOT NULL UNIQUE,
  CONSTRAINT Cookies_pk PRIMARY KEY ("id")
) WITH (
OIDS=FALSE
);
--alter-----------------------------------------------------------------------------
ALTER TABLE "Cookies" ADD CONSTRAINT Cookies_fk0 FOREIGN KEY ("user_id") REFERENCES "Users"("id");

ALTER TABLE "Artists" ADD CONSTRAINT "Artists_fk0" FOREIGN KEY ("main_photo") REFERENCES "Artist_photo"("id");

ALTER TABLE "News" ADD CONSTRAINT "News_fk0" FOREIGN KEY ("main_photo") REFERENCES "News_photos"("id");

ALTER TABLE "News_photos" ADD CONSTRAINT "News_photos_fk0" FOREIGN KEY ("news_id") REFERENCES "News"("id");

ALTER TABLE "Artist_photo" ADD CONSTRAINT "Artist_photo_fk0" FOREIGN KEY ("artist_id") REFERENCES "Artists"("id");

ALTER TABLE "Artist_video" ADD CONSTRAINT "Artist_video_fk0" FOREIGN KEY ("artist_id") REFERENCES "Artists"("id");

ALTER TABLE "User_links" ADD CONSTRAINT "User_links_fk0" FOREIGN KEY ("user_id") REFERENCES "Users"("id");

ALTER TABLE "Admins" ADD CONSTRAINT "Admins_fk0" FOREIGN KEY ("user_id") REFERENCES "Users"("id");

ALTER TABLE "Topicks" ADD CONSTRAINT "Topicks_fk0" FOREIGN KEY ("initial_comment") REFERENCES "Comments"("id");

ALTER TABLE "Comments" ADD CONSTRAINT "Comments_fk0" FOREIGN KEY ("user_id") REFERENCES "Users"("id");
ALTER TABLE "Comments" ADD CONSTRAINT "Comments_fk1" FOREIGN KEY ("topick_id") REFERENCES "Topicks"("id");


ALTER TABLE "Tags_in_topicks" ADD CONSTRAINT "Tags_in_topicks_fk0" FOREIGN KEY ("tag_id") REFERENCES "Tags"("id");
ALTER TABLE "Tags_in_topicks" ADD CONSTRAINT "Tags_in_topicks_fk1" FOREIGN KEY ("topick_id") REFERENCES "Topicks"("id");



--insert-----------------------------------------------------------------------------


INSERT INTO "Users" (username,name,password,city,about,img_url)
values('bad','evgen','comedian','moscow','hate jokes','../../img/bad.jpg');

INSERT INTO "Admins" (user_id)
values(1);

INSERT INTO "Artists" (name,info_url)
values('[BadComedian]','hate, jokes, irony');

INSERT INTO "Artist_photo" (artist_id,content_url)
values(1,'../../img/bad1.jpg');

UPDATE "Artists" SET main_photo=1 WHERE id=1;

INSERT INTO "Topicks" (title)
values('who admin?')









