package service.db.dao;

import java.util.List;

/**
 * Created by lucky.mz on 06.11.2017.
 */
public interface iMediaDAO {

    List<String> getArtistPhotoPath(int id);
    String getNewsPhotoPath(int id);

    String getMainArtPhoto(int mainPhoto_id);
    String getNewMainPhoto(int photo_id);

    boolean insertToNews(int news_id, String path);
    boolean insertToArtist(int art_id, String path);

    String getArtVideo(int art_id);
    boolean insertArtVideo(int art_id, String path);
}
