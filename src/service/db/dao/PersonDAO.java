package service.db.dao;



import entities.Person;
import entities.UserPass;
import service.db.DBHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonDAO implements iPersonDAO {

    private Connection conn = DBHelper.getConnection();

    @Override
    public List<String> getPersons(String q) {
        String req = "select username from \"Users\" WHERE username = ?" ;
        List<String> usernames = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(req);
            st.setString(1,q);
            ResultSet rs = st.executeQuery();

            while (rs.next()){
                usernames.add(
                    rs.getString("username")
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usernames;
    }

    @Override
    public Map<String, String> getUsers() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select * from \"Users\""
            );

            Map<String,String> users = new HashMap<>();
            while (rs.next()){
                users.put(
                        rs.getString("username"),
                        rs.getString("password")
                );
            }
            return users;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Person getById(int person_id) {
        try {
            String req = "SELECT * FROM \"Users\" WHERE id =" + person_id;
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(req);
            List<Person> persons = new ArrayList<>();
            while (rs.next()) {
                Person person = new Person(
                        rs.getString("username"),
                        rs.getInt("id"),
                        rs.getString("img_url"),
                        rs.getString("name"),
                        rs.getString("about"),
                        rs.getString("city")
                );
                persons.add(person);
            }
            return persons.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean insert(Person person, String password) {
        String request = "INSERT INTO \"Users\" (username, password, name," +
                "city,about,img_url) VALUES (?,?,?,?,?,?)";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setString(1, person.getUsername());
            st.setString(2, password);
            st.setString(3, person.getRealName());
            st.setString(4, person.getCity());
            st.setString(5, person.getDescription());
            st.setString(6, person.getPhoto());
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public UserPass getUsPass(String username) {
        String request = "SELECT username, password FROM \"Users\" WHERE username= ? ";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setString(1, username);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                 return new UserPass(resultSet.getString("username"),
                        resultSet.getString("password")
                );
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    @Override
    public Person getByUsName(String username) {
        String request = "SELECT * FROM \"Users\" WHERE username= ? ";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Person(
                        rs.getString("username"),
                        rs.getInt("id"),
                        rs.getString("img_url"),
                        rs.getString("name"),
                        rs.getString("about"),
                        rs.getString("city")
                );
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isAdmin(int user_id) {
        String request = "SELECT * FROM \"Admins\" WHERE user_id= ? ";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setInt(1, user_id);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean addAdmin(String username) {
        int user_id = getByUsName(username).getId();
        String request = "INSERT INTO \"Admins\" (user_id) VALUES (?)";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setInt(1, user_id);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}

