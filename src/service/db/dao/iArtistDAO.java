package service.db.dao;

import entities.Artist;

import java.util.List;

/**
 * Created by lucky.mz on 06.11.2017.
 */
public interface iArtistDAO {

    List<Artist> getArtists();
    Artist getById(int art_id);

    boolean deleteBy(int id);
}
