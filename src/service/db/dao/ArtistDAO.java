package service.db.dao;

import entities.Artist;
import service.db.DBHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucky.mz on 06.11.2017.
 */
public class ArtistDAO implements iArtistDAO{

    private Connection conn = DBHelper.getConnection();

    @Override
    public List<Artist> getArtists() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from \"Artists\"");

            List<Artist> artists = new ArrayList<>();
            while (rs.next()){
                Artist artist = new Artist(
                        rs.getString("name"),
                        rs.getString("info_url"),
                        rs.getInt("id"),
                        rs.getInt("main_photo")
                );
                artists.add(artist);

            }
            return artists;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Artist getById(int art_id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from \"Artists\" WHERE id =" + art_id);

            List<Artist> artists = new ArrayList<>();
            while (rs.next()){
                  Artist artist = new Artist(
                          rs.getString("name"),
                          rs.getString("info_url"),
                          rs.getInt("id"),
                          rs.getInt("main_photo")
                );
                artists.add(artist);

            }
            return artists.get(0);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public boolean deleteBy(int id) {
        String dell = "DELETE FROM \"Artists\" WHERE id = ? ";
        try {
            PreparedStatement st = conn.prepareStatement(dell);
            st.setInt(1,id);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
