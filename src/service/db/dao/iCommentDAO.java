package service.db.dao;


import entities.Comment;

import java.util.List;

public interface iCommentDAO {

    List<Comment> getCommentsFromTopicId(int topic_id);

    Comment getById(int first_comment_id);

    boolean insert(String comment, int user_id, int topic_id);

    boolean deleteBy(int id);
}
