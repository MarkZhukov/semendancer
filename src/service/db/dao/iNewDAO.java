package service.db.dao;


import entities.NewsPost;

import java.util.List;

public interface iNewDAO {

    List<NewsPost> getNews();
    void deleteBy(int id);
}
