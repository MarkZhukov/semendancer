package service.db.dao;

import entities.Comment;
import entities.ForumTopic;
import entities.Person;
import service.db.DBHelper;

import javax.swing.event.ListDataListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucky.mz on 06.11.2017.
 */
public class ForumTopicDAO implements iForumTopicDAO {

    private Connection conn = DBHelper.getConnection();

    @Override
    public List<ForumTopic> getAsList() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select * from \"Topicks\"");

            List<ForumTopic> topics = new ArrayList<>();
            while (rs.next()){
                ForumTopic topic = new ForumTopic(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("initial_comment")
                );
                topics.add(topic);
            }
            return topics;
        } catch (SQLException e) {
            return null;
        }
    }



    @Override
    public ForumTopic getById(int id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select * from \"Topicks\" " +
                            "WHERE id=" + id);
            List<ForumTopic> topics = new ArrayList<>();
            while (rs.next()) {
                ForumTopic topic = new ForumTopic(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("initial_comment")
                );
                topics.add(topic);
            }
            return topics.get(0);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public boolean insert(String title, String text, String[] tag, int user_id) {
        String insert_topic = "INSERT INTO \"Topicks\" (title) VALUES (?)";
        String topic_id_req = "SELECT id FROM \"Topicks\" WHERE title= ? ";
        String coment_id_req = "SELECT id FROM \"Comments\" WHERE " +
                "content = ? AND user_id = ? AND topick_id = ? ";
        String insert_init_com = "UPDATE \"Topicks\" SET initial_comment = ? WHERE id = ? ";
        int topic_id;
        int comment_id;
        try {
            PreparedStatement st = conn.prepareStatement(insert_topic);
            st.setString(1, title);
            st.executeUpdate();

            st = conn.prepareStatement(topic_id_req);
            st.setString(1, title);
            ResultSet rs = st.executeQuery();
            if (rs.next()){
                topic_id = rs.getInt("id");
                CommentDAO dao = new CommentDAO();
                dao.insert(text,user_id,topic_id);

                st = conn.prepareStatement(coment_id_req);
                st.setString(1,text);
                st.setInt(2,user_id);
                st.setInt(3,topic_id);
                rs = st.executeQuery();
                if (rs.next()) {
                    comment_id = rs.getInt("id");

                st = conn.prepareStatement(insert_init_com);
                st.setInt(1,comment_id);
                st.setInt(2,topic_id);
                st.executeUpdate();

                addTags(tag,topic_id);
                return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<ForumTopic> getByWord(String search) {
        String request = "SELECT id FROM \"Topicks\" WHERE title LIKE ? ";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setString(1, "%" + search + "%");
            List<Integer> topics_id = new ArrayList<>();
            List<ForumTopic> topics = new ArrayList<>();
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                topics_id.add(rs.getInt("id"));
            }
            for (Integer i : topics_id) {
                topics.add(getById(i));
            }
            return topics;
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ForumTopic> getByTeg(String teg) {
        String request = "SELECT id FROM \"Tags\" WHERE tag= ? ";
        int tag_id = -1;
        List<Integer> topics_id = new ArrayList<>();
        List<ForumTopic> topics = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setString(1, teg);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                tag_id = rs.getInt("id");
            }
            if (tag_id == -1){
                return null;
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        String patt = "SELECT topick_id FROM \"Tags_in_topicks\"" +
                "WHERE tag_id= ? ";
        try {
            PreparedStatement st = conn.prepareStatement(patt);
            st.setInt(1, tag_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                topics_id.add(rs.getInt("topick_id"));
            }
            for (Integer i : topics_id) {
                topics.add(getById(i));
            }
            return topics;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean addTags(String[] tag, int topic_id) {
        String chek_uniq = "SELECT id FROM \"Tags\" WHERE tag = ? ";
        String ins_tag = "INSERT INTO \"Tags\" (tag) VALUES ( ? )";
        try {

            PreparedStatement st;
            ResultSet rs;
            int tag_id = -1;
            for (int i = 0; i < tag.length; i++) {
                st = conn.prepareStatement(chek_uniq);
                st.setString(1, tag[i]);
                rs = st.executeQuery();
                if (rs.next()){
                    tag_id = rs.getInt("id");
                }else {
                    st = conn.prepareStatement(ins_tag);
                    st.setString(1, tag[i]);
                    st.executeUpdate();

                    st = conn.prepareStatement(chek_uniq);
                    st.setString(1, tag[i]);
                    rs = st.executeQuery();
                    if (rs.next()) {
                        tag_id = rs.getInt("id");
                    }
                }
                System.out.println(tag_id);
                if (tag_id > 0) return addTag_in_Topic(tag_id,topic_id);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean addTag_in_Topic(int tag_id, int topic_id) {
        String insert = "INSERT INTO \"Tags_in_topicks\" " +
                "(tag_id,topick_id) VALUES (?,?)";
        try {
            PreparedStatement st = conn.prepareStatement(insert);
            st.setInt(1,tag_id);
            st.setInt(2,topic_id);
            st.execute();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteBy(int id) {
        String dell = "DELETE FROM \"Topicks\" WHERE id = ?";
        String dell_comment = "DELETE FROM \"Comments\" WHERE topick_id = ? ";
        try {
            PreparedStatement st = conn.prepareStatement(dell_comment);
            st.setInt(1,id);
            st.executeUpdate();
            st = conn.prepareStatement(dell);
            st.setInt(1,id);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


}
