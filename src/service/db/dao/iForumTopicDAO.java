package service.db.dao;

import entities.ForumTopic;

import java.util.List;

/**
 * Created by lucky.mz on 06.11.2017.
 */
public interface iForumTopicDAO {
    List<ForumTopic> getAsList();
    ForumTopic getById(int id);
    boolean insert(String title, String text, String[] tag, int user_id);
    List<ForumTopic> getByWord(String search);
    List<ForumTopic> getByTeg(String teg);
    boolean addTags(String[] tag, int topic_id);
    boolean addTag_in_Topic(int tag_id, int topic_id);
    boolean deleteBy(int id);
}
