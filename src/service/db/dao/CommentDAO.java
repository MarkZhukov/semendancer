package service.db.dao;

import entities.Comment;
import service.db.DBHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucky.mz on 06.11.2017.
 */
public class CommentDAO implements iCommentDAO {

    private Connection conn = DBHelper.getConnection();

    @Override
    public List<Comment> getCommentsFromTopicId(int topic_id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM \"Comments\" WHERE topick_id=" +
                            topic_id
            );
            List<Comment> comments = new ArrayList<>();
            while (rs.next()){
                Comment comment = new Comment(
                        rs.getInt("id"),
                        rs.getString("content"),
                        rs.getInt("user_id"),
                        rs.getInt("topick_id")
                        //, rs.getLong("date_time")
                );
                comments.add(comment);
            }
            return comments;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Comment getById(int comment_id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM \"Comments\" WHERE id=" + comment_id
            );
            if (rs.next()) {
                 return  new Comment(
                        rs.getInt("id"),
                        rs.getString("content"),
                        rs.getInt("user_id"),
                        rs.getInt("topick_id")
                        //,rs.getLong("date_time")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insert(String comment, int user_id, int topic_id) {
        String request = "INSERT INTO \"Comments\" (user_id,content,topick_id) VALUES (?,?,?)";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setInt(1, user_id);
            st.setString(2, comment);
            st.setInt(3, topic_id);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteBy(int id) {
        String dell = "DELETE FROM \"Comments\" WHERE id = ?";
        try {
            PreparedStatement st = conn.prepareStatement(dell);
            st.setInt(1,id);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}