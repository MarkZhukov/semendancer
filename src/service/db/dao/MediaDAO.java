package service.db.dao;

import service.db.DBHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucky.mz on 06.11.2017.
 */
public class MediaDAO implements iMediaDAO {

    private Connection conn = DBHelper.getConnection();

    @Override
    public List<String> getArtistPhotoPath(int id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select content_url from \"Artist_photo\" " +
                            "WHERE artist_id=" + id
            );
            List<String> photos = new ArrayList<>();
            while (rs.next()){
                String s = rs.getString("content_url");
                photos.add(s);
            }
            return photos;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public String getNewsPhotoPath(int id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select photo_url from \"News_photos\" " +
                            "WHERE news_id=" + id
            );

            if (rs.next()){
                return rs.getString("photo_url");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getMainArtPhoto(int mainPhoto_id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select content_url from \"Artist_photo\" " +
                            "WHERE id=" + mainPhoto_id
            );
            List<String> strings = new ArrayList<>();
            if (rs.next()) {
                return rs.getString("content_url");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getNewMainPhoto(int photo_id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select photo_url from \"News_photos\"" +
                            "WHERE id=" + photo_id
            );
            if (rs.next()) {
                return rs.getString("photo_url");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insertToNews(int news_id, String path) {
        String request = "INSERT INTO \"News_photos\" (news_id, photo_url) VALUES (?,?)";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setInt(1, news_id);
            st.setString(2, path);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean insertToArtist(int art_id, String path) {
        String request = "INSERT INTO \"Artist_photo\" (arist_id, content_url) VALUES (?,?)";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setInt(1, art_id);
            st.setString(2, path);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String getArtVideo(int art_id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select content_url from \"Artist_video\"" +
                            "WHERE id=" + art_id
            );
            if (rs.next()) {
                return rs.getString("content_url");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insertArtVideo(int art_id, String path) {
        String request = "INSERT INTO \"Artist_video\" (arist_id, content_url) VALUES (?,?)";
        try {
            PreparedStatement st = conn.prepareStatement(request);
            st.setInt(1, art_id);
            st.setString(2, path);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
