package service.db.dao;

import entities.UserPass;

/**
 * Created by lucky.mz on 17.11.2017.
 */
public interface iCookieDAO {
    Object[] compareValue(String decode);
    String add(UserPass usps);
    boolean deleteCookie(int user_id);
}
