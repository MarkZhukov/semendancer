package service.db.dao;

import entities.Person;
import entities.UserPass;
import service.db.DBHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by lucky.mz on 17.11.2017.
 */
public class CookieDAO implements iCookieDAO {

    private Connection conn = DBHelper.getConnection();

    @Override
    public Object[] compareValue(String hashCookie) {
        String select = "SELECT user_id FROM \"Cookies\" WHERE hash = ? ";
        try {
            PreparedStatement st = conn.prepareStatement(select);
            st.setString(1,hashCookie);
            ResultSet rs = st.executeQuery();
            if (rs.next()){
                PersonDAO dao = new PersonDAO();
                return new Object[]{dao.getById(rs.getInt("user_id")),hashCookie};
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String add(UserPass usps) {
        PersonDAO person = new PersonDAO();
        Person p = person.getByUsName(usps.getUsername());
        String insert = "INSERT INTO \"Cookies\" (user_id,hash)" +
                "VALUES (?,?)";
        try {
            PreparedStatement st = conn.prepareStatement(insert);
            st.setInt(1,p.getId());
            st.setString(2,usps.hashCookie());
            st.executeUpdate();
            return usps.hashCookie();
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteCookie(int user_id) {
        String dell = "DELETE FROM \"Cookies\" WHERE user_id = ? ";
        try {
            PreparedStatement st = conn.prepareStatement(dell);
            st.setInt(1,user_id);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
