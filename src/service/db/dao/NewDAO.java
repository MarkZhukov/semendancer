package service.db.dao;


import entities.NewsPost;
import service.db.DBHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class NewDAO implements iNewDAO {

    private Connection conn = DBHelper.getConnection();

    @Override
    public List<NewsPost> getNews() {

        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from \"News\"");
            List<NewsPost> news = new ArrayList<>();
            while (rs.next()){
                NewsPost n = new NewsPost(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("content_url"),
                        rs.getString("date"),
                        rs.getInt("main_photo")
                );
                news.add(n);
            }
            return news;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteBy(int id) {
        String dell = "DELETE FROM \"News\" WHERE id = ? ";
        try {
            PreparedStatement st = conn.prepareStatement(dell);
            st.setInt(1,id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
