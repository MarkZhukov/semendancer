package service.db.dao;


import entities.Person;
import entities.UserPass;

import java.util.List;
import java.util.Map;

public interface iPersonDAO {

    List<String> getPersons(String q);
    Map<String,String> getUsers();
    Person getById(int person_id);
    boolean insert(Person person, String password);
    UserPass getUsPass(String username);
    Person getByUsName(String username);
    boolean isAdmin(int user_id);
    boolean addAdmin(String username);
}
