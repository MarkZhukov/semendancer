package Pages;

import entities.Person;
import service.db.dao.CookieDAO;
import service.db.dao.PersonDAO;
import service.db.dao.iCookieDAO;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * Created by lucky.mz on 17.11.2017.
 */
public class CookieRulet implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        if (((HttpServletRequest) req).getSession().getAttribute("current_user") == null) {
            Cookie[] cookies = ((HttpServletRequest) req).getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (!cookie.getName().equals("remember")) continue;
                    CookieDAO dao = new CookieDAO();
                    Object[] o = dao.compareValue(URLDecoder.decode(cookie.getValue(), "UTF-8"));
                    if (o != null) {
                        ((HttpServletRequest) req).getSession().setAttribute("current_user", o[0]); //login
                        ((HttpServletRequest) req).getSession().setAttribute("isLogIn", new Integer(1)); //login
                        PersonDAO personDAO = new PersonDAO();
                        Person person = (Person)((HttpServletRequest) req).getSession().getAttribute("current_user");
                        if (personDAO.isAdmin(person.getId())){
                            ((HttpServletRequest) req).getSession().setAttribute("isAdmin", new Integer(1)); //login
                        }
                        cookie.setValue((String) o[1]);
                        cookie.setMaxAge(60 * 60 * 24 * 365); //1 year
                        ((HttpServletResponse) resp).addCookie(cookie);
                        break;
                    }
                }
            }
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
