package Pages;

import entities.Person;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.PersonDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class ViewPersonPage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s[] = request.getRequestURI().split("/");
        int id = Integer.parseInt(s[s.length - 1]);
        if (request.getSession().getAttribute("current_user") != null) {
            Template template = ConfigSingleton.getConfig(
                    request.getServletContext()
            ).getTemplate("edit_person.ftl");
            Map<String, Object> root = new HashMap<>();

            PersonDAO dao = new PersonDAO();
            Person person = dao.getById(id);

            root.put("p",person);
            root.put("isLogIn",
                    request.getSession().getAttribute("isLogIn")
            );
            root.put("is_admin",
                    request.getSession().getAttribute("isAdmin")
            );


            try {
                template.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }else response.sendRedirect("/login");

    }
}
