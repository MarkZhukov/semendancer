package Pages;

import entities.Person;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.ForumTopicDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lucky.mz just in case.
 */
public class CreateTopicPage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Person user = (Person) request.getSession().getAttribute("current_user");
        String title = request.getParameter("title");
        String text = request.getParameter("text");
        String tags = request.getParameter("tags");
        String s[] = tags.split(" ");
        tags = "";
        for (String value : s) {
            tags = tags + value;
        }
        response.getWriter().print(tags + "\n");
        s = tags.split("#");
        String tag[] = new String[s.length - 1];
        for (int i = 0; i < tag.length; i++) {
            tag[i] = s[i + 1];
        }
        response.getWriter().print(Arrays.toString(s));
        response.getWriter().print(Arrays.toString(tag));
        pushInDB(title, text, tag, user.getId());


        response.sendRedirect("/forum");
    }

    private boolean pushInDB(String title, String text, String[] tag, int user_id) {
        ForumTopicDAO dao = new ForumTopicDAO();
        return dao.insert(title,text,tag,user_id);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("current_user") != null) {
            Template template = ConfigSingleton.getConfig(
                    request.getServletContext()
            ).getTemplate("create_topic.ftl");
            Map<String,String> root = new HashMap<>();
            if (request.getSession().getAttribute("current_user") != null) {
                root.put("us",
                        ((Person)request.getSession()
                                .getAttribute("current_user")).getUsername());
            }
            try {
                template.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }else response.sendRedirect("/forum");



       // response.setContentType("text/html");
       // response.getWriter().print("<h1>Creating Topic</h1>" +
       //        "<a href=\"/home\">home</a>\n"
       //        + "<p> form</p>"
        //
       // );
    }
}
