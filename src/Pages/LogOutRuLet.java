package Pages;

import entities.Person;
import service.db.dao.CookieDAO;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by lucky.mz in kitchen
 */
public class LogOutRuLet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CookieDAO dao = new CookieDAO();
        dao.deleteCookie(((Person)request.getSession().getAttribute("current_user")).getId());
        request.getSession().setAttribute("current_user", null);
        request.getSession().setAttribute("isAdmin", new Integer(0));
        request.getSession().setAttribute("isLogIn", new Integer(0));
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie == null || !cookie.getName().equals("remember")) continue;
                cookie.setValue(null);
            }
        }
        response.sendRedirect("/home");
    }
}
