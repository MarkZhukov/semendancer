package Pages;

import entities.ForumTopic;
import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import service.ConfigSingleton;
import service.db.dao.ForumTopicDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by lucky.mz onClick.
 */
public class ForumListPage extends HttpServlet {
    private String search;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search = request.getParameter("search");
            doGet(request,response);
    }

    private List<ForumTopic> searchTitle(String search) {
        ForumTopicDAO dao = new ForumTopicDAO();
        return dao.getByWord(search);
    }

    private List<ForumTopic> searchTeg(String teg) {
        ForumTopicDAO dao = new ForumTopicDAO();
        return dao.getByTeg(teg.toLowerCase());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
        Template tmpl = cfg.getTemplate("forum_list.ftl");
        Map<String, Object> root = new HashMap<>();

        List<ForumTopic> topics;
        if (search == null || search.equals("")) {
            topics = getTopicsFromDB();
        }else {
            root.put("search",search);
            if (search.charAt(0) == '#') {
                search = search.split("#")[1];
                topics = searchTeg(search);
            }else {
                topics = searchTitle(search);}
            search = null;
        }
        root.put("topics", topics);
        root.put(
                "isLogIn",
                request.getSession().getAttribute("isLogIn")
        );
        root.put(
                "is_admin",
                request.getSession().getAttribute("isAdmin")
        );
        if (request.getSession().getAttribute("current_user") != null) {
            root.put("us",
                    ((Person)request.getSession()
                            .getAttribute("current_user")).getUsername());
        }
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }

        String search = request.getParameter("search");
        if (search == null) search = "#%$#@#$@#$$#@@#$%#@!@#$%^%$@!!!";
        // узнать у БД фамилии студентов, в которых есть q
        ForumTopicDAO dao = new ForumTopicDAO();
        List<String> titles = new ArrayList<>();
        for(ForumTopic topic: dao.getByWord(search)) {
            titles.add(topic.getTitle());
        }
        // построить json по ResultSet
        JSONObject jo = new JSONObject();
        JSONArray ja = new JSONArray();

        for (ForumTopic topic: dao.getByWord(search)) {
            ja.put(topic.getTitle());
        }
        try {
            jo.put("topics", ja);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // передать json в response
        response.setContentType("text/json");
        //response.getWriter().print(jo.toString());
        response.getWriter().close();



    }

    private List<ForumTopic> getTopicsFromDB() {
        ForumTopicDAO dao = new ForumTopicDAO();
        return dao.getAsList();
    }
}
