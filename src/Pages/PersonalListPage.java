package Pages;

import entities.Artist;
import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.ArtistDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lucky.mz on event
 */
public class PersonalListPage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
        Template tmpl = cfg.getTemplate("personal_list.ftl");
        Map<String, Object> root = new HashMap<>();

        root.put(
                "isLogIn",
                request.getSession().getAttribute("isLogIn")
        );
        root.put(
                "is_admin",
                request.getSession().getAttribute("isAdmin")
        );
        root.put("artistes", getListArtistDB());

        if (request.getSession().getAttribute("current_user") != null) {
            root.put("us",
                    ((Person)request.getSession()
                            .getAttribute("current_user")).getUsername());
        }
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }

    }

    private List<Artist> getListArtistDB() {
         ArtistDAO dao = new ArtistDAO();
        return dao.getArtists();
    }
}
