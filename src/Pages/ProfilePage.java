package Pages;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.PersonDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by
 */
public class ProfilePage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s[] = request.getRequestURI().split("/");
        String username = s[s.length - 1];
        if (username != null && !username.isEmpty()) {
            Template template = ConfigSingleton.getConfig(
                    request.getServletContext()
            ).getTemplate("profile.ftl");
            Map<String, Object> root = new HashMap<>();
            PersonDAO dao = new PersonDAO();
            root.put("person", dao.getByUsName(username));
            root.put(
                    "is_admin",
                    request.getSession().getAttribute("isAdmin")
            );
            root.put(
                    "isLogIn",
                    request.getSession().getAttribute("isLogIn")
            );


            try {
                template.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }
        else {
            response.sendRedirect("/login");
        }
    }

}
