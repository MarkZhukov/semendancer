package Pages;

import com.ibm.useful.http.*;
import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import service.ConfigSingleton;
import service.Hash;
import service.db.dao.PersonDAO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Created by lucky.mz on cher.
 */
public class SignInPage extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (isMultipartFormat(request)) {
                PostData formData = new PostData(request);
                String username = formData.getParameter("username").toLowerCase();
                PersonDAO dao = new PersonDAO();
                if (dao.getByUsName(username) != null){
                    doGet(request,response);
                    response.getWriter().print("<script>alert('that username already exist');</script>");
                }
                String password1 = formData.getParameter("password1");
                String password2 = formData.getParameter("password2");
                if (!password1.equals(password2)) {
                    doGet(request, response);
                    response.getWriter().print("<script>alert('password not equals confirm');</script>");
                } else {
                    String realName = formData.getParameter("realName");
                    if (checkReg(realName)) {
                        String description = formData.getParameter("about");
                        String city = formData.getParameter("city");
                        String inst = formData.getParameter("inst");
                        String twitter = formData.getParameter("twit");
                        String facebook = formData.getParameter("face");
                        String vk = formData.getParameter("vk");

                         FileData tempFile = formData.getFileData("photo");
                            String filename = tempFile.getFileName();
                            if (filename != null) {
                                saveFile(tempFile);
                                filename = "../../img/" + filename;
                            } else filename = null;
                            Person person = new Person();
                            person.setUsername(username);
                            person.setRealName(realName);
                            person.setCity(city);
                            person.setDescription(description);
                        if(isLink(facebook)) person.setFaceLink(facebook);
                            person.setPhoto(filename);
                        if(isLink(inst)) person.setInstLink(inst);
                        if(isLink(twitter)) person.setTwitLink(twitter);
                        if(isLink(vk)) person.setVkLink(vk);


                            request.getSession().setAttribute("current_user", person);
                            request.getSession().setAttribute("isLogIn", new Integer(1));
                            request.getSession().setAttribute("isAdmin", new Integer(0));
                                    putInDbThisBastard(
                                            person,
                                            Hash.getMd5Apache(password1)
                                    );

                            response.sendRedirect("/profile/" +
                                    ((Person)request.getSession().getAttribute("current_user")).getUsername()
                            );

                    }else {
                        doGet(request,response);
                        response.getWriter().print("<script>alert('name should have only letters');</script>");
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isLink(String link) {
        return Pattern.compile("http:/(.)*").matcher(link).matches();
    }

    private boolean checkReg(String realName) {
        if (realName.isEmpty()) return false;
        Pattern pattern = Pattern.compile("([A-Z]*[a-z]*)+");
        return pattern.matcher(realName).matches();
    }


    //функция, проверяющая пришел ли запрос в формате multipart
    private boolean isMultipartFormat(HttpServletRequest req) throws javax.servlet.ServletException, java.io.IOException {
        String temptype = req.getContentType();
        if(temptype.indexOf("multipart/form-data")!=-1) return true;
        else return false;
    }

    //функция, сохраняющая пришедший файл на диск
    private void saveFile(FileData tempFile) throws IOException {
        File f = new File("C:\\DanceRep\\semendancer\\web\\img\\" + tempFile.getFileName());
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(tempFile.getByteData());
        fos.close();
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("isLogIn").equals(0)) {
            Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
            Template tmpl = cfg.getTemplate("sign_up.ftl");
            try {
                tmpl.process(null, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }else response.sendRedirect("/profile");

    }

    private boolean putInDbThisBastard(Person person, String password) {
        PersonDAO dao = new PersonDAO();
        return dao.insert(person,password);
    }
}
