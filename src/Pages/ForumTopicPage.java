package Pages;

import entities.Comment;
import entities.ForumTopic;
import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.CommentDAO;
import service.db.dao.ForumTopicDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Created by lucky.mz on 22.10.2017.
 */
public class ForumTopicPage extends HttpServlet {

    private Person current_user;
    private ForumTopic topic;
    private List<Comment> comments;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String comment = request.getParameter("comment");
        current_user = (Person) request.getSession().getAttribute("current_user");
        addNewCommentDB(comment, current_user.getId(), topic.getId());
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s[] = request.getRequestURI().split("/");
        int topic_id = Integer.parseInt(s[s.length - 1]);

        Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
        Template tmpl = cfg.getTemplate("forum_topic.ftl");
        Map<String, Object> root = new HashMap<>();
        current_user = (Person) request.getSession().getAttribute("current_user");
        topic = getTopicDB(topic_id);
        comments = getCommentsDB(topic_id);
        root.put("topic",topic);
        root.put("comments", comments);
        root.put("us", current_user);
        root.put(
                "isLogIn",
                request.getSession().getAttribute("isLogIn")
        );
        root.put(
                "is_admin",
                request.getSession().getAttribute("isAdmin")
        );


        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }


    private List<Comment> getCommentsDB(int topic_id) {
        CommentDAO dao = new CommentDAO();
        return dao.getCommentsFromTopicId(topic_id);
    }

    private ForumTopic getTopicDB(int topic_id) {
        ForumTopicDAO dao = new ForumTopicDAO();
        return dao.getById(topic_id);
    }

    private void addNewCommentDB(String comment, int user_id, int topic_id) {
        CommentDAO dao = new CommentDAO();
        dao.insert(comment,user_id,topic_id);
    }
}
