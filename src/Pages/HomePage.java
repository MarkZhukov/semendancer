package Pages;

import entities.NewsPost;
import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.ForumTopicDAO;
import service.db.dao.NewDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lucky.mz on just in time
 */
public class HomePage extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //response.setContentType("text/html");
        Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
        Template tmpl = cfg.getTemplate("news.ftl");
        Map<String, Object> root = new HashMap<>();
        root.put(
                "isLogIn",
                request.getSession().getAttribute("isLogIn")
        );
        root.put(
                "is_admin",
                request.getSession().getAttribute("isAdmin")
        );
        if (request.getSession().getAttribute("current_user") != null) {
            root.put("us",
                    ((Person) request.getSession()
                            .getAttribute("current_user")).getUsername());
        }
        List<NewsPost> news = getNewsDB();
        if (news.isEmpty()) System.out.println("empty");
        root.put("new",news);
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    private List<NewsPost> getNewsDB() {
        NewDAO dao = new NewDAO();
        return dao.getNews();
    }
}
