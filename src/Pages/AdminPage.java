package Pages;

import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.PersonDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lucky.mz on 27.10.2017.
 */
public class AdminPage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String username = request.getParameter("username");
        if (!username.isEmpty()) {
            makeAdmin(username);
            response.sendRedirect("/home");
        }else {
            doGet(request,response);
        }
    }

    private boolean makeAdmin(String username) {
        PersonDAO dao = new PersonDAO();
        if (dao.getByUsName(username) != null) {
            if (!dao.isAdmin(dao.getByUsName(username).getId())){
                dao.addAdmin(username);
                return true;
            }
        }
        return false;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("isAdmin").equals(1)) {
            Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
            Template tmpl = cfg.getTemplate("admin.ftl");
            Map<String, String> root = new HashMap<>();
            if (request.getSession().getAttribute("current_user") != null) {
                root.put("us",
                        ((Person) request.getSession()
                                .getAttribute("current_user")).getUsername());
            }

            try {
                tmpl.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }else response.sendRedirect("/home");
    }
}