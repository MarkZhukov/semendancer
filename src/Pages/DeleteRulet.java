package Pages;

import service.db.dao.ArtistDAO;
import service.db.dao.CommentDAO;
import service.db.dao.ForumTopicDAO;
import service.db.dao.NewDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by lucky.mz on 19.11.2017.
 */
public class DeleteRulet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s[] = request.getRequestURI().split("/");
        int id = Integer.parseInt(s[s.length - 1]);
        String table = s[s.length - 2];
        switch (table){
            case "art":
                ArtistDAO dao = new ArtistDAO();
                dao.deleteBy(id);
                response.sendRedirect("/artist_list");
                System.out.println("art dell");
                break;
            case "topic":
                ForumTopicDAO forumTopicDAO = new ForumTopicDAO();
                forumTopicDAO.deleteBy(id);
                response.sendRedirect("/forum");
                System.out.println("topic dell");
                break;
            case "news":
                NewDAO newDAO = new NewDAO();
                newDAO.deleteBy(id);
                response.sendRedirect("/home");
                System.out.println("news dell");
                break;
            case "comment":
                CommentDAO commentDAO = new CommentDAO();
                commentDAO.deleteBy(id);
                response.sendRedirect("/forum");
                System.out.println("comment dell");
                break;
            default:
                response.sendRedirect("/home");
                break;
        }

    }
}
