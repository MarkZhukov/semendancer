package Pages;

import entities.Person;
import entities.UserPass;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.Hash;
import service.db.DBHelper;
import service.db.dao.CookieDAO;
import service.db.dao.PersonDAO;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by lucky.mz not really.
 */
public class LoginPage extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username").toLowerCase();
        String password = request.getParameter("password");
        boolean rememberMe = request.getParameter("remember") != null;
        password = Hash.getMd5Apache(password);
        UserPass usps = new UserPass(username,password);
        Person person;
        if (compareData(usps)){
            person = getCurrentUserDB(username);
            request.getSession().setAttribute(
                    "current_user", person
            );
            request.getSession().setAttribute(
                    "isLogIn", new Integer(1)
            );
            if (isAdmin(person.getId())) {
                request.getSession().setAttribute("isAdmin", new Integer(1));
            }
            if (rememberMe) {
                CookieDAO dao = new CookieDAO();
                Cookie cookie = new Cookie("remember",
                        URLEncoder.encode(dao.add(usps), "UTF-8"));
                cookie.setMaxAge(60*60*24*365); //1 year
                cookie.setSecure(false);
                response.addCookie(cookie);
                System.out.println("cookie added");
            }
            response.sendRedirect("/profile/" +
                    ((Person)request.getSession().getAttribute("current_user")).getUsername());
        } else {
            response.sendRedirect("/login ");
            //response.sendRedirect("/login?err=Wrong Username&username=" + username);
        }
    }




    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("isLogIn").equals(0)) {
            Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
            Template tmpl = cfg.getTemplate("log_in.ftl");
            try {
                tmpl.process(null, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
            //response.setContentType("text/html");
            //response.getWriter().print("<h1>log in</h1>" +
            //        "<a href=\"/home\">home</a>\n");
            //String err = request.getParameter("err");
            //String username = request.getParameter("username");
            //response.getWriter().println(
            //        (err != null ? "Error: " + err + "<br/>" : "") +
            //                "<form method=\"POST\" action=\"/login\">\n" +
            //                "    <input type=\"text\" name=\"username\" " +
            //                (username != null ? "value='" + username + "' " : "") +
            //                "/>\n" +
            //                "    <input type=\"password\" name=\"password\"/>\n" +
            //                "    <input type=\"submit\" value=\"login\"/>\n" +
            //                "</form>" +
            //                "<a href=\"/signIn\">sign in</a>");
        }else response.sendRedirect("/profile/" +
                ((Person)request.getAttribute("current_user")).getUsername());
    }


    private boolean compareData(UserPass usps) {
        PersonDAO dao = new PersonDAO();
        return usps.equals(dao.getUsPass(usps.getUsername()));
    }

    private Person getCurrentUserDB(String username) {
        PersonDAO dao = new PersonDAO();
        return dao.getByUsName(username);
    }

    private boolean isAdmin(int user_id) {
        PersonDAO dao = new PersonDAO();
        return dao.isAdmin(user_id);
    }
}
