package Pages;

import entities.Artist;
import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.db.dao.ArtistDAO;
import service.db.dao.MediaDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lucky.mz on sad.
 */
public class ArtistPage extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s[] = request.getRequestURI().split("/");
        int artist_id = Integer.parseInt(s[s.length - 1]);
        Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
        Template tmpl = cfg.getTemplate("artist.ftl");
        Map<String, Object> root = new HashMap<>();

        Artist artist = getArtist(artist_id);
        List<String> photos = getListArtistPhoto(artist_id);
        String oh = artist.getMainPhoto();
        for (int i = 0; i < photos.size(); i++) {
            if (photos.get(i).equals(oh)) {
                photos.remove(i);
            }
        }
        oh = "";
        if (!photos.isEmpty()){
            oh = photos.get(0);
            photos.remove(0);
        }

        String videoURL = getArtistVideo(artist_id);
        root.put("artist", artist);
        root.put("photos", photos);
        root.put("video", videoURL);
        root.put("tit", oh);
        root.put(
                "isLogIn",
                request.getSession().getAttribute("isLogIn")
        );
        root.put(
                "is_admin",
                request.getSession().getAttribute("isAdmin")
        );
        if (request.getSession().getAttribute("current_user") != null) {
            root.put("us",
                    ((Person) request.getSession()
                            .getAttribute("current_user")).getUsername());
        }
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    private String getArtistVideo(int artist_id) {
        MediaDAO dao = new MediaDAO();
        return dao.getArtVideo(artist_id);
    }

    private List<String> getListArtistPhoto(int artist_id) {
        MediaDAO dao = new MediaDAO();
        return dao.getArtistPhotoPath(artist_id);
    }

    private Artist getArtist(int artist_id) {
        ArtistDAO dao = new ArtistDAO();
        return dao.getById(artist_id);
    }
}
