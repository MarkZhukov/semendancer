package Pages;

import entities.Person;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import service.ConfigSingleton;
import service.Hash;
import service.db.dao.CommentDAO;
import service.db.dao.ForumTopicDAO;
import service.db.dao.PersonDAO;
import service.db.dao.MediaDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by lucky.mz on nedavno
 */
public class StartPage extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/home");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("current_user") == null) {
            Configuration cfg = ConfigSingleton.getConfig(request.getServletContext());
            Template tmpl = cfg.getTemplate("get_in.ftl");

            //response.getWriter().print(
            //        "<h3>some fucking beuty scribe</h3>\n" +
            //        "<form action=\"/start\" method=\"POST\">\n" +
            //        "<input type=\"submit\" value=\"get_in\">\n" +
            //        "</form>" +
            //                "<p> "
            //                + commentDAO.getCommentsFromTopicId(1).get(1).getDescription()
            //                + "</p>"
            //);
            request.getSession().setAttribute(
                    "isLogIn", new Integer(0)
            );
            request.getSession().setAttribute(
                    "isAdmin", new Integer(0)
            );
            try {
                tmpl.process(null, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }else response.sendRedirect("/home");
    }
}
